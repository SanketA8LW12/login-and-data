/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/


const path = require('path');
const fs = require('fs');
const { error } = require('console');


function createFile(fileName){
    return new Promise((resolve, reject)=>{
        fs.writeFile(fileName, "The file has been created", (error)=>{
            if(error){
                reject(error);
            }
            else{
                resolve();
            }
        })
    })
} 


function deleteFile(fileName){
    return new Promise((resolve, reject)=>{
        fs.unlink(fileName, (error)=>{
            if(error){
                reject(error);
            }
            else{
                resolve();
            }
        })
    })
}



Promise.all([
    createFile('./file1.txt'),
    createFile('./file2.txt')
  ]).then(()=>{
    console.log("Two files has been created");
  }).catch((error)=>{
    console.error("The file creating error : " + error);
  });


function fileDeleting(){
    deleteFile('./file1.txt')
.then(()=>{
    console.log("File one has been deleted");
    deleteFile('./file2.txt')
}).then(()=>{
    console.log("File two has been deleted");
}).catch((error=>{
    console.error('Error :', error);
}))
}

setTimeout(fileDeleting, 2000);